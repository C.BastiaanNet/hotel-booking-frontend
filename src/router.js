import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Explore from './views/Explore.vue'
import contact from './views/contact.vue'
import booking from './views/booking.vue'
import booking1 from './components/booking1.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/explore',
      name: 'explore',
      component: Explore,
      props: true,
    },
    {
      path: '/contact',
      name: 'contact',
      component: contact
    },
    {
      path: '/booking/',
      name: 'booking',
      component: booking,
    },
  ]
})
