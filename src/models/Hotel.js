import Room from "@/models/Room.js";

export default class Hotel{

	constructor(){
		this.name = '';
		this.description = '';
		this.address = '';		
		this.price = 0;
		this.stars = 0;
		this.id = 0;
		this.rooms = [];
		this.facilities = [];
		this.reviews = [];
	}

	static fromJson(json){
		const self = new Hotel();

		self.id = json.id;
		self.name = json.name;
		self.description = json.description;
		self.address = json.address;
		self.rooms = json.rooms.map(Room.fromJson);
		self.stars = json.stars;
		self.facilities = json.availableFacilities;
		self.reviews = json.reviews;

		var lowprice = self.rooms[0].price;
		for(let i=1;i<json.rooms.length;i++){
			if(lowprice > self.rooms[i].price){
				lowprice = self.rooms[i].price;				
			}
		}
		self.price = lowprice;

		return self;
	}
}