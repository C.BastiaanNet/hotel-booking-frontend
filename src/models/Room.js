export default class Room{

	constructor(){
		this.id = 0;
		this.name = '';
		this.image = '';
		this.price = 0;
		this.size = 0;
		this.bed = '';
		this.quantity = 0;
		this.facilities = [];
		this.availableFacilities = [];
	}

	static fromJson(json){
		const self = new Room();

		self.id = json.id;
		self.name = json.name;
		self.image = json.image;
		self.price = json.price;
		self.size = json.size;
		self.bed = json.bed;
		self.quantity = json.quantity;
		self.facilities = json.facilities;
		self.availableFacilities = json.availableFacilities;

		return self;
	}

	static emptyRoom(){
		const self = new Room();

		self.name = "Please select a room";

		return self;
	}

	equalize(json){
		self = this;
		self.id = json.id;
		self.name = json.name;
		self.image = json.image;
		self.price = json.price;
		self.size = json.size;
		self.bed = json.bed;
		self.quantity = json.quantity;
		self.facilities = json.facilities;
		self.availableFacilities = json.availableFacilities;		
	}
}